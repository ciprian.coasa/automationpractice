<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkTermsAndConditions</name>
   <tag></tag>
   <elementGuidId>4fd7aac0-0f16-4bee-b8a3-1a6de8821e84</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Terms and conditions of use']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Terms and conditions of use&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0d91fedd-a541-45e9-a3df-320de78d17d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_cms=3&amp;controller=cms</value>
      <webElementGuid>4782ee67-b9ed-4500-bad1-d26c99a84d1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Terms and conditions of use</value>
      <webElementGuid>fe478c58-8705-41e1-8b4b-03df098a1096</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
							Terms and conditions of use
						</value>
      <webElementGuid>b1d965ca-817a-473b-aa07-b1ba9d48f952</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block_various_links_footer&quot;)/ul[@class=&quot;toggle-footer&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
      <webElementGuid>c204d9e9-bc9e-4f71-9293-53d4e88a108d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='block_various_links_footer']/ul/li[6]/a</value>
      <webElementGuid>4db6bac2-ed34-4804-8418-9a31c12dfb74</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Terms and conditions of use')]</value>
      <webElementGuid>a13dc364-8956-4a5d-9eb3-d93106e047ef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Contact us'])[2]/following::a[1]</value>
      <webElementGuid>7f3155fc-6c8d-4b67-961c-1f329d810b30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Our stores'])[1]/following::a[2]</value>
      <webElementGuid>3fe8e972-3980-4379-91fc-cd07691e5d6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About us'])[1]/preceding::a[1]</value>
      <webElementGuid>67e7078f-01dd-4e42-8a43-0a3ffcca7a2c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sitemap'])[1]/preceding::a[2]</value>
      <webElementGuid>b317813c-3926-4628-9395-2a06c4512172</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Terms and conditions of use']/parent::*</value>
      <webElementGuid>10784111-fa81-4cbc-8be1-c49b7c5bd384</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?id_cms=3&amp;controller=cms']</value>
      <webElementGuid>c0141aac-8eb5-4824-8367-99e04c98ef56</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[6]/a</value>
      <webElementGuid>0a1bab18-0a07-4a4e-b846-7eb01c09cb3e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_cms=3&amp;controller=cms' and @title = 'Terms and conditions of use' and (text() = '
							Terms and conditions of use
						' or . = '
							Terms and conditions of use
						')]</value>
      <webElementGuid>5a76bb0b-3bae-4ea3-adfc-4901d19ec169</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
