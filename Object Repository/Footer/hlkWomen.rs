<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkWomen</name>
   <tag></tag>
   <elementGuidId>fc01679b-d3bc-4213-9355-e231085110dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(@title, 'all woman fashion collections')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title*=&quot;all woman fashion collections&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>386fab5d-b91e-418f-b1a0-b9d5bd196b79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?id_category=3&amp;controller=category</value>
      <webElementGuid>794227d8-fbdb-44ad-af55-bcc0687ede99</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>You will find here all woman fashion collections.  
 This category includes all the basics of your wardrobe and much more: 
 shoes, accessories, printed t-shirts, feminine dresses, women's jeans!</value>
      <webElementGuid>c2b99b40-2156-43a1-80d3-26dd544bbe25</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
		Women
	</value>
      <webElementGuid>1ad31ec6-076c-4263-b499-ad601c3705b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;footer&quot;)/div[@class=&quot;row&quot;]/section[@class=&quot;blockcategories_footer footer-block col-xs-12 col-sm-2&quot;]/div[@class=&quot;category_footer toggle-footer&quot;]/div[@class=&quot;list&quot;]/ul[@class=&quot;tree dynamized&quot;]/li[@class=&quot;last&quot;]/a[1]</value>
      <webElementGuid>11538f1a-f8e5-47ec-b1ac-8440d8192e6d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//footer[@id='footer']/div/section[2]/div/div/ul/li/a</value>
      <webElementGuid>46540fb2-b149-4947-96ff-b8d8b6669f7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'Women')])[2]</value>
      <webElementGuid>e1dfbabc-8fb1-4d7c-9552-ed198626065f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Categories'])[2]/following::a[1]</value>
      <webElementGuid>05b52080-f7a7-47fc-8d78-e78805df0285</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Follow us'])[1]/following::a[1]</value>
      <webElementGuid>f0cb0b7a-772f-4206-996f-3446d5932337</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tops'])[2]/preceding::a[1]</value>
      <webElementGuid>14d732a5-ba93-4768-bd1f-d6971d33c1ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='T-shirts'])[3]/preceding::a[2]</value>
      <webElementGuid>0f1c7238-5a0a-4d19-9a80-4c3eba48755e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[@href='http://automationpractice.com/index.php?id_category=3&amp;controller=category'])[2]</value>
      <webElementGuid>566075f3-9482-4fa7-a927-2e59fbd6ba4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[2]/div/div/ul/li/a</value>
      <webElementGuid>9ff4a7a2-a4f9-4b31-8144-1a696b03c942</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?id_category=3&amp;controller=category' and @title = concat(&quot;You will find here all woman fashion collections.  
 This category includes all the basics of your wardrobe and much more: 
 shoes, accessories, printed t-shirts, feminine dresses, women&quot; , &quot;'&quot; , &quot;s jeans!&quot;) and (text() = '
		Women
	' or . = '
		Women
	')]</value>
      <webElementGuid>74757318-5899-4708-9812-255fd4f31772</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
