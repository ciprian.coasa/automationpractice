<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkNewProducts</name>
   <tag></tag>
   <elementGuidId>db351508-f456-4457-adf5-4ff6b27561fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='New products']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;New products&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>d1f490b4-8b1c-4877-88d3-f5e22a20022d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=new-products</value>
      <webElementGuid>fc3fe8b6-5236-4c47-9fa1-ad12da9957eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>New products</value>
      <webElementGuid>24e2d09b-30de-46a0-a60b-ed45d89a9e6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
					New products
				</value>
      <webElementGuid>abaf8c87-728b-4205-9907-df4d8207f452</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;block_various_links_footer&quot;)/ul[@class=&quot;toggle-footer&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
      <webElementGuid>aaff3f4d-317c-42e7-8442-9ff3b46e651c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='block_various_links_footer']/ul/li[2]/a</value>
      <webElementGuid>f944c94d-dc71-4223-a9dd-f6c164591823</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'New products')]</value>
      <webElementGuid>1b9e828a-1f5e-4971-a134-7bf5e039ad31</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Specials'])[1]/following::a[1]</value>
      <webElementGuid>36b058b5-1a67-422e-bcf7-27abe2bddf83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Information'])[1]/following::a[2]</value>
      <webElementGuid>30dfd0a3-1bbc-410c-b7c5-e0141499201c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Best sellers'])[1]/preceding::a[1]</value>
      <webElementGuid>e25de231-034b-4fb0-87bb-d57dbfabc640</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Our stores'])[1]/preceding::a[2]</value>
      <webElementGuid>69d2e548-1e34-4c31-b579-b85985a0d403</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='New products']/parent::*</value>
      <webElementGuid>92706542-2615-4360-8c85-5800d13ee2f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://automationpractice.com/index.php?controller=new-products']</value>
      <webElementGuid>1fe0e69d-f9d3-4d3b-be7f-89f64979e36e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section[3]/ul/li[2]/a</value>
      <webElementGuid>32ab4150-c378-478c-8f7f-d4bf5520c1db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://automationpractice.com/index.php?controller=new-products' and @title = 'New products' and (text() = '
					New products
				' or . = '
					New products
				')]</value>
      <webElementGuid>6f5015ed-48dc-462c-9f20-0ef940ad8c45</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
