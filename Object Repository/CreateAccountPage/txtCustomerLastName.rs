<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtCustomerLastName</name>
   <tag></tag>
   <elementGuidId>c7113dae-2224-483d-8dbd-87a1dccad87e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#customer_lastname</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='customer_lastname']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
