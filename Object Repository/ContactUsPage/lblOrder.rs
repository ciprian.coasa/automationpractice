<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lblOrder</name>
   <tag></tag>
   <elementGuidId>2fc20836-9878-47b6-888f-f9863b01df81</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//label[text()='Order reference']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>label+input#id_order</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>label</value>
      <webElementGuid>23871484-0ef5-4801-9a32-f0e1926b0c1e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Order reference</value>
      <webElementGuid>49211a2f-d974-4758-94ce-6f236b97452c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;center_column&quot;)/form[@class=&quot;contact-form-box&quot;]/fieldset[1]/div[@class=&quot;clearfix&quot;]/div[@class=&quot;col-xs-12 col-md-3&quot;]/div[@class=&quot;form-group selector1&quot;]/label[1]</value>
      <webElementGuid>1642dd8e-f175-46e8-b305-a9cf77a3cb3b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='center_column']/form/fieldset/div/div/div[2]/label</value>
      <webElementGuid>c1c49678-aed7-44d9-8f56-758cb89cdb83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email address'])[1]/following::label[1]</value>
      <webElementGuid>a2983510-c134-43fc-a520-a25192a42f13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-- Choose --'])[1]/following::label[2]</value>
      <webElementGuid>1426eebf-7044-4eab-a77c-1f7db77affb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Attach File'])[1]/preceding::label[1]</value>
      <webElementGuid>3e1f4256-04c3-47e9-b24d-a32d02001bc7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='No file selected'])[1]/preceding::label[2]</value>
      <webElementGuid>ff5401f3-3906-4474-8565-32d68e81af84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Order reference']/parent::*</value>
      <webElementGuid>0458c8d6-f9b0-469c-b926-d650901b896c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/label</value>
      <webElementGuid>49485a6b-de8c-41ec-87e6-a2bd676d7c78</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//label[(text() = 'Order reference' or . = 'Order reference')]</value>
      <webElementGuid>ae5411fd-1afd-4716-8d6a-29e7d4c2596b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
