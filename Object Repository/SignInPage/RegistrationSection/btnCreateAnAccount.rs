<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnCreateAnAccount</name>
   <tag></tag>
   <elementGuidId>b8ebb3f9-922c-4502-a341-c44fa6448751</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='SubmitCreate']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#SubmitCreate</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>6a89aa5e-0e93-465d-8079-7791cfbb8bd1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								
								Create an account
							</value>
      <webElementGuid>dce89387-e093-403b-803c-12804c9270c8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SubmitCreate&quot;)/span[1]</value>
      <webElementGuid>bf15f4a3-d8af-4daa-ab36-6a614418b025</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='SubmitCreate']/span</value>
      <webElementGuid>5ae818c7-36da-4504-b623-3fb5fd694790</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email address'])[1]/following::span[1]</value>
      <webElementGuid>4d5c4560-2547-4a58-9143-a834a3fa4f5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create an account'])[1]/following::span[1]</value>
      <webElementGuid>7dc964a7-81c8-42f7-8f97-ffd65b3c422c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Already registered?'])[1]/preceding::span[1]</value>
      <webElementGuid>39e90f6b-69d4-447c-888e-b67f026e4d33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email address'])[2]/preceding::span[1]</value>
      <webElementGuid>711b6ec6-d346-4944-80da-afffdbc95929</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/button/span</value>
      <webElementGuid>ccf0ee13-7f85-47b7-9656-937911b99a1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
								
								Create an account
							' or . = '
								
								Create an account
							')]</value>
      <webElementGuid>9b5847fe-b6d6-4b66-ba2f-bed725bc986e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
