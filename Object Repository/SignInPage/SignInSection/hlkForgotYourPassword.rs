<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hlkForgotYourPassword</name>
   <tag></tag>
   <elementGuidId>289ea648-10e4-4094-b279-80529fe96f64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@title='Recover your forgotten password']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[title=&quot;Recover your forgotten password&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>d578bbed-ce9b-449e-b6e8-702aac85bf8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>lost_password form-group</value>
      <webElementGuid>55f9c194-cffc-4805-b733-2afd6f53afac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Forgot your password?</value>
      <webElementGuid>f52c1ff5-a5aa-41b9-aff8-7b2903c07c66</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login_form&quot;)/div[@class=&quot;form_content clearfix&quot;]/p[@class=&quot;lost_password form-group&quot;]</value>
      <webElementGuid>055b5f5d-f3b5-49b6-b87a-1c032731da23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login_form']/div/p</value>
      <webElementGuid>db957e02-86ce-4eb2-be89-bb0461ff408e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::p[1]</value>
      <webElementGuid>7d0a035c-4fa7-475f-a8db-f399a5b02ea8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email address'])[2]/following::p[1]</value>
      <webElementGuid>3d86c566-8880-4dd3-aa43-a968e241c3b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/form/div/p</value>
      <webElementGuid>b0d62503-b78c-40cd-b1da-8a87368c1b73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Forgot your password?' or . = 'Forgot your password?')]</value>
      <webElementGuid>83c032cf-1b7c-43e4-8738-135a0a937da9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
