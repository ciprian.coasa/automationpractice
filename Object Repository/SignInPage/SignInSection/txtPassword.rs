<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>txtPassword</name>
   <tag></tag>
   <elementGuidId>dd68702c-94d0-4ad6-8246-6b02dac9a760</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='passwd']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#passwd</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>3e98dcd7-4932-4c0b-9b23-dbb73c274dcb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>is_required validate account_input form-control</value>
      <webElementGuid>40575beb-64cd-4e0e-bdd0-43c9c79c85e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>password</value>
      <webElementGuid>d9464079-4bdb-4a26-8957-42354230eec0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-validate</name>
      <type>Main</type>
      <value>isPasswd</value>
      <webElementGuid>991ffccd-5da2-405f-bd31-5c26582d6b05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>passwd</value>
      <webElementGuid>eecade2a-c807-4b54-8921-a5d28de67080</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>passwd</value>
      <webElementGuid>47b21002-01fc-43cf-bd6d-2e9ee6c99947</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;passwd&quot;)</value>
      <webElementGuid>ff1b6b11-51fd-4edb-b1c9-debbd0faf2f0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='passwd']</value>
      <webElementGuid>7b6c205b-6f6b-460c-858c-70921f989b08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login_form']/div/div[2]/span/input</value>
      <webElementGuid>4fdfd536-7283-4b76-8be5-6576c605fcb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/input</value>
      <webElementGuid>e702e0ce-8a2b-461b-b485-b3726e3cb3ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'password' and @id = 'passwd' and @name = 'passwd']</value>
      <webElementGuid>a75072fd-5deb-45b2-9b2b-541d7e881d7a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
