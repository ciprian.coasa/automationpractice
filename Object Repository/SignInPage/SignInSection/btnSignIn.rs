<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnSignIn</name>
   <tag></tag>
   <elementGuidId>3fda3bc9-c59c-4466-bbef-3b14eb3a6a2e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='SubmitLogin']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#SubmitLogin</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>98c811fa-6782-493a-9a93-0f803ac45777</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
								
								Sign in
							</value>
      <webElementGuid>4fcdb5a2-40c2-4c2f-bd9f-3ab544d19a44</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SubmitLogin&quot;)/span[1]</value>
      <webElementGuid>eea6f1f7-6fc4-4931-b264-94881db340fc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//button[@id='SubmitLogin']/span</value>
      <webElementGuid>292f540b-fc45-4eca-bac3-f8b7a2fc505b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Forgot your password?'])[1]/following::span[1]</value>
      <webElementGuid>473e40e1-3c26-4d5a-be0b-793726fcc740</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/following::span[2]</value>
      <webElementGuid>85dba1e9-5606-4c3b-9574-f8be0f5d2559</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Newsletter'])[1]/preceding::span[1]</value>
      <webElementGuid>e81aeda6-284f-41da-b1d3-8dad7066d3e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ok'])[1]/preceding::span[1]</value>
      <webElementGuid>5cf0c108-ee82-422b-aaaf-52718ff0b5f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//p[2]/button/span</value>
      <webElementGuid>a9525e2c-f58d-4e2f-8266-6b965671fc7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
								
								Sign in
							' or . = '
								
								Sign in
							')]</value>
      <webElementGuid>6550de6b-1aac-413e-b909-ea8cc0224ef5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
